"""This module contains the logic to validate JSON object against a JSON schema"""

from typing import Any
from jsonschema import validate, ValidationError


def is_valid(instance: object, schema: Any) -> bool:
    """Validates an object against a schema"""

    try:
        validate(instance=instance, schema=schema)
    except ValidationError:
        return False
    return True


def are_schemas_equivalent(schema1: Any, schema2: Any, instance: object):
    """We assume two semantically equivalent schemas would validate the same object"""
    return is_valid(instance, schema1) and is_valid(instance, schema2)


def find_and_validate_schemas(d: Any, schemas: dict[str, Any]) -> bool:
    """Look for and validate relevant schemas in configuration"""
    for key, value in d.items():
        if key in schemas:
            if not are_schemas_equivalent(
                value, schemas[str(key)][0], schemas[str(key)][1]
            ):
                print(
                    "Mismatched schemas:\nConfiguration schema:\n",
                    value,
                    "\nProvided schema:\n",
                    schemas[key],
                )
                return False
            else:
                continue
        if isinstance(value, dict):
            return find_and_validate_schemas(value, schemas)
    return True
