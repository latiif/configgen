"""This module defines the Cataloguer class"""
from typing import Dict, List
from os.path import join

from treqs.list_elements import list_elements
from treqs.treqs_element import treqs_element
from treqs.check_elements import check_elements


DEFAULT_TTIM_FILENAME = "ttim.yaml"


class Cataloguer:
    """This class keeps a lookup directory of all the elements in the model and their properties"""

    def __init__(self, directory: str):
        if not self.is_model_problem_free(directory=directory):
            raise RuntimeError(f"The model in {directory} is inconsistent. Abort!")

        le = list_elements()
        le.get_element_list(
            file_name=directory, treqs_type=None, inlinks=True, recursive=True
        )
        self._catalogue: Dict[str, treqs_element] = _map_objects_to_dict(
            le.element_list.copy()
        )

    def is_model_problem_free(self, directory: str) -> bool:
        """Checks for consistency of Treqs elements"""
        checker = check_elements()
        try:
            checker.check_elements(
                file_name=directory,
                recursive=True,
                ttim_path=join(directory, DEFAULT_TTIM_FILENAME),
            )
        except SystemExit as se:
            return se.code == 0  # Exit code of 0 indicates success
        return False  # check_elements() must produce a SystemExit call

    def get_catalogue(self, treqs_type: str | None = None) -> Dict[str, treqs_element]:
        """Retrieve a catalogue of all elements, or elements of type `treqs_type`"""
        if treqs_type is None:
            return self._catalogue.copy()

        return {
            uid: element
            for uid, element in self._catalogue.items()
            if element.treqs_type == treqs_type
        }


def _map_objects_to_dict(elements: List[treqs_element]) -> Dict[str, treqs_element]:
    return {el.uid: el for el in elements}
