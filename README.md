# GRAMS - Generic Runtime Abstraction Modeler and Selector

GRAMS is a configuration generation tool built on top of TReqs + Compositional Architectural Framework.


## Overview

Leveraging T-Reqs-modelled architectural descriptions, the tool ensures traceable configuration generation, incorporating both functional and non-functional requirements. The resulting configuration specification includes values, dynamic properties needing adjustment, and their rationale, providing an intermediary format adaptable for system integration or specific targets.

## Installation

Clone the repository and install the dependencies using pip:

```bash
git clone https://gitlab.com/latiif/configgen.git
cd configgen
pip install -e .
```

## Usage

To use GRAMS, execute the `grams` command followed by appropriate options:

```bash
grams -s <path_to_configuration_schema> [-d <directory_path>] [-p <processor_type>]
```

### Options

- `-s, --schema`: Path to the configuration schema file (required).
- `-d, --dir`: Path to the directory containing the TReqs based VEDLIoT
  framework instance (default: current directory).
- `-p, --processor`: Processor function type (default: yaml). Available choices:
  [yaml, uml].

## Example

```bash
grams -s schema.json -d /path/to/directory -p uml
```

[![asciicast](https://asciinema.org/a/7XY73Q9nIez6XSsbCgbZ5bDwE.svg)](https://asciinema.org/a/7XY73Q9nIez6XSsbCgbZ5bDwE)

## License

This project is licensed under the MIT License.

## Author

Abdullatif AlShriaf (latiif@mail.com)
