"""The main module and acts as the entry point for GRAMS"""

from argparse import ArgumentParser

from processor import get_processors, ProcessingFunction
from cataloguer import Cataloguer
from tracer import Tracer
from type_matcher import TypeMatcher
from json import loads


def main(
    directory: str,
    configuration_schema_path: str,
    processing_function: ProcessingFunction,
) -> None:
    """Main entry point function of GRAMS"""

    with open(configuration_schema_path) as f:
        configuration_schema_raw = f.read()
        f.close()
        try:
            configuration_schema = loads(configuration_schema_raw)
        except RuntimeError as e:
            print("Unable to load configuration schema:", e)
            return
    cataloguer: Cataloguer = Cataloguer(directory=directory)
    tracer = Tracer(directory=dir, catalogue=cataloguer.get_catalogue())
    type_matcher = TypeMatcher(
        directory=directory, catalogue=cataloguer.get_catalogue()
    )
    runtime_scenarios = cataloguer.get_catalogue(treqs_type="runtime-scenario")
    for runtime_scenario in runtime_scenarios.values():
        for level_of_abstraction in runtime_scenario.outlinks:
            processing_function(
                level_of_abstraction=level_of_abstraction,
                schema=configuration_schema,
                catalogue=cataloguer.get_catalogue(),
                tracer=tracer,
                type_matcher=type_matcher,
            )


def grams():
    parser = ArgumentParser(
        description="GRAMS - A configuration generation tool built on top of TReqs + VEDLIoT"
    )
    parser.add_argument(
        "-d",
        "--dir",
        help="Path to directory containing the TReqs based VEDLIOT framework instance",
        dest="directory",
        default=".",
    )
    parser.add_argument(
        "-p",
        "--processor",
        help="Processor function",
        dest="processor",
        choices=list(get_processors()),
        default="yaml",
    )
    parser.add_argument(
        "-s",
        "--schema",
        help="Path to configuration schema",
        dest="configuration_schema_path",
        required=True,
    )

    args = parser.parse_args()
    main(
        directory=args.directory,
        configuration_schema_path=args.configuration_schema_path,
        processing_function=get_processors()[args.processor],
    )


if __name__ == "__main__":
    grams()
