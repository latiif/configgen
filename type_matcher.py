"""This module defines the TypeMatcher class"""

import re
import yaml
from treqs.list_elements import list_elements

from treqs.treqs_element import treqs_element
from validator import is_valid, find_and_validate_schemas


def extract_objects(raw_string: str) -> object:
    """Extract YAML/JSON object(s) from string"""

    matches = re.finditer(
        r"```(json|yaml)(.*?)```", raw_string, re.DOTALL | re.IGNORECASE
    )
    for match in matches:
        yaml_str = match.group(2)
        try:
            yaml_obj = yaml.safe_load(yaml_str)
            return yaml_obj
        except (yaml.YAMLError, yaml.scanner.ScannerError):
            print(f"Unable to parse: `{yaml_str}`.")


class TypeMatcher:
    """TypeMatcher matches the types of embedlInput elements to schema-type elements"""

    def __init__(self, directory: str, catalogue: dir):
        self.directory = directory
        self.catalogue = catalogue

    def extract_schema_types(self) -> dict[str, (str, str)]:
        """Produce a dictionary that maps embedlInput id's to their schema type and schema"""
        le = list_elements()
        le.get_element_list(
            file_name=self.directory,
            treqs_type="schema-type",
            outlinks=True,
            recursive=True,
        )
        lookup_table = {}
        for element in le.element_list:
            for embedl_input in element.outlinks:
                embedl_input_id = self.catalogue[embedl_input.target].uid
                schema = extract_objects(self.catalogue[element.uid].text)
                schema_type = schema["title"]
                object_instance = extract_objects(self.catalogue[embedl_input_id].text)
                if is_valid(instance=object_instance, schema=schema):
                    lookup_table[embedl_input_id] = (schema_type, schema)
                else:
                    print(
                        "Invalid JSON:\n",
                        object_instance,
                        "\naccording to schema:\n",
                        schema,
                    )
                    raise SystemExit(1)
        return lookup_table

    def check_schema_mismatches(
            self,
            configuration_schema: object,
    ) -> bool:
        """Iterate over configuration schema
        looking for mismatches between relevant embedlInput schemas"""

        lookup = dict()
        for key, value in self.extract_schema_types().items():
            object_instance = extract_objects(self.catalogue[key].text)
            lookup[value[0]] = (value[1], object_instance)

        return find_and_validate_schemas(configuration_schema, schemas=lookup)
