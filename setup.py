"""The setup module for the GRAMS tool"""
from setuptools import setup

with open("requirements.txt", encoding="utf-8") as f:
    required = f.read().splitlines()

setup(
    name="grams",
    py_modules=["."],
    version="0.1.0",
    install_requires=required,
    packages=["."],
    entry_points={
        "console_scripts": [
            "grams = main:grams",
        ],
    },
)
