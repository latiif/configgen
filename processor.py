"""Processes the elements and traces to represent them in different ways"""

import hashlib
import json
from textwrap import fill
from typing import Callable

import yaml
from treqs.treqs_element import treqs_element

from config import Configuration, EmbedlInputInfo, PathNode, TracePath
from tracer import Tracer
from type_matcher import TypeMatcher, extract_objects


def strip_python_tags(s: str) -> str:
    """Remove YAML tags from yaml string"""
    result = []
    for line in s.splitlines():
        idx = line.find("!!python/")
        if idx > -1:
            line = line[:idx]
        result.append(line)
    return "\n".join(result)


ProcessingFunction = (
    Callable[
        [treqs_element, object, dict[str, treqs_element], Tracer, TypeMatcher], None
    ],
)


def get_processors() -> dict[str, ProcessingFunction]:
    """maps string to a processor function"""
    return {
        "uml": process_as_plant_uml,
        "yaml": process_as_yaml,
    }


def process_as_yaml(
    level_of_abstraction: treqs_element,
    schema: object,
    catalogue: dict[str, treqs_element],
    tracer: Tracer,
    type_matcher: TypeMatcher,
):
    """Traverses the information model and lists out relevant paths as YAML"""
    tracer.traverse(start=catalogue[level_of_abstraction.target])
    schema_types_lookup_table = type_matcher.extract_schema_types()
    if not type_matcher.check_schema_mismatches(configuration_schema=schema):
        raise SystemExit(1)

    config = Configuration(schema)
    for path in tracer.get_path().values():
        trace_path: TracePath = []
        for n in path:
            trace_path.append(
                PathNode(
                    uid=n.uid,
                    treqs_type=n.treqs_type,
                    filename=n.file_name,
                    line=n.placement,
                )
            )
            if n.treqs_type == "embedlInput":
                config.embedl_inputs.append(
                    EmbedlInputInfo(n, trace_path, schema_types_lookup_table[n.uid])
                )
    print(strip_python_tags(yaml.dump(config, default_flow_style=False)))


def process_as_plant_uml(
    level_of_abstraction: treqs_element,
    schema: object,
    catalogue: dict[str, treqs_element],
    tracer: Tracer,
    type_matcher: TypeMatcher,
):
    """Traverses the information model and lists out the relevant paths as PlantUML"""

    def hash_string(string: str) -> str:
        colors = [
            "Application",
            "Business",
            "Implementation",
            "Motivation",
            "Physical",
            "Strategy",
            "Technology",
        ]

        # Use hashlib to create a hash object
        hash_object = hashlib.sha256(string.encode())

        # Get the hexadecimal representation of the hash
        hex_hash = hash_object.hexdigest()

        # Convert the hexadecimal to an integer and perform modulo to get a number between 0 and 10
        result = int(hex_hash, 32) % len(
            colors
        )  # 11 is used instead of 10 to include 10 in the possible range
        return colors[result]

    tracer.traverse(start=catalogue[level_of_abstraction.target])
    schema_types_lookup_table = type_matcher.extract_schema_types()
    i = 0
    print("@startuml")
    print("left to right direction")
    for path in tracer.get_path().values():
        print(f"label l{i} [")
        print("{{")
        for n in path:
            short_uid = n.uid[:8]

            print(
                f'object "{short_uid}" as {short_uid}{i} #{hash_string(n.treqs_type)}'
            )
            print(f"{short_uid}{i} : ~**{n.treqs_type}**")
            print(f"{short_uid}{i} : __{fill(n.label,width=20)}__".replace("\n", "\\n"))
            if n.treqs_type == "embedlInput":
                print(
                    f"{short_uid}{i} : \\n{(json.dumps(extract_objects(n.text),indent=2))}".replace(
                        "\n", "\\n"
                    )
                )
        for j in range(len(path) - 1):
            print(f"{path[j].uid[:8]}{i} -> {path[j+1].uid[:8]}{i}")
        print("}}")
        print("]")
        i = i + 1

    print("legend bottom")
    print("|**embedlInput**|**schema type**|")
    for embedl_input, (schema_type, _) in schema_types_lookup_table.items():
        print(f"|//{embedl_input[:8]}// | {schema_type}|")
    print("end legend")
    print("@enduml")
