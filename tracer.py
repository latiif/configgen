"""Tracer module defines the Tracer class"""

from typing import Dict, List, Set
from treqs.treqs_element import treqs_element


class Tracer:
    """Tracer tracks all possible paths between a level of abstraction and embedlInput elements"""

    def __init__(self, directory: str, catalogue: Dict[str, treqs_element]):
        self.directory = directory
        self.catalogue = catalogue
        self.paths: List[List[treqs_element]] = []

    def traverse(self, start, visited=None, path=None):
        """traverses the nodes via their inlinks, and registers the path along the way"""

        if visited is None:
            visited = []
        if path is None:
            path = []

        if start.uid in visited:
            return path, False
        visited.append(start.uid)
        path.append(start)
        if start.treqs_type == "embedlInput":
            return path, True
        if not start.inlinks:
            return path, False

        for inlink in start.inlinks:
            possible_path, good = self.traverse(
                start=self.catalogue[inlink.source],
                visited=visited.copy(),
                path=path.copy(),
            )
            if good:
                self.paths.append(possible_path)

        return path, False

    def get_path(self) -> Dict[str, List[treqs_element]]:
        """Returns the paths to all found embedlInput elements in the model"""

        def get_number_of_requirements_in_path(path: List[treqs_element]) -> int:
            return len([True for p in path if p.treqs_type == "requirement"])

        paths_dict: Dict[str, List[treqs_element]] = {}
        embedl_input_ids: Set[str] = set()
        for path in self.paths:
            embedl_input_ids.add(path[-1].uid)

        for embedl_input_element_uid in embedl_input_ids:
            paths_dict[embedl_input_element_uid] = []
            paths = [p for p in self.paths if p[-1].uid == embedl_input_element_uid]
            ranked = sorted(paths, key=get_number_of_requirements_in_path, reverse=True)
            if len(ranked) > 1:
                paths_dict[embedl_input_element_uid] = ranked[0]

        return paths_dict
